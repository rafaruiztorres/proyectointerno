global class creaRegistroVacacionesNuevoAnyo implements Schedulable {

	global void execute(SchedulableContext ctx) {
		//Job que se realizará cada 1 de Enero a las 5 de la mañana para crear el registro de vacaciones del año.

		//Recojo todos los usuarios del sistema
		List<User> userList = [ SElECT ID,FirstName,LastName FROM USER WHERE isActive = true ];

		System.Debug('********** Lista usuarios: ' + userList);

		List<Vacaciones__c> listVacaciones = new List<Vacaciones__c>();

		Vacaciones__c vacacionNueva;
		Vacaciones__c vacacionAntigua;

		Set<Id> setIds = new Set<Id>();

		Map<Id,Vacaciones__c> mIdVacaciones = new Map<Id,Vacaciones__c>();
		Integer diasVacacionesNoUsados=0;
		
		//Relleno mi set con todos los ID de los usuarios.
		for (User u: userList){
			setIds.add(u.Id);
		}

		//Cogemos las vacaciones del año anterior.
		for (Vacaciones__c vacacion : [ SELECT Id, Empleado__c,Empleado__r.Fecha_de_incorporacion__c,Empleado__r.FirstName,Empleado__r.LastName,Activo__c,Dias_de_vacaciones__c,Anyo__c,Name,Dias_por_consumir__c FROM Vacaciones__c  WHERE Empleado__c in :setIds AND anyo__c = :Date.today().year()-1 ]){
			mIdVacaciones.put(vacacion.Empleado__c,vacacion);
		}

		System.Debug('Mapa de vacaciones:' + mIdVacaciones);
		System.Debug('Set de IDs:' + setIds);

		//Si el usuario tuvo vacaciones el año anterior, las desactivamos, cogemos los días que le sobran y se los añadimos al nuevo año.

		for(User u: userList){
			vacacionNueva = new Vacaciones__c(Empleado__c = u.Id, Activo__c = true);
			if(mIdVacaciones.containsKey(u.Id)){
				vacacionAntigua = mIdVacaciones.get(u.Id);
				vacacionAntigua.Activo__c = false;
				diasVacacionesNoUsados = Integer.valueOf(vacacionAntigua.Dias_por_consumir__c);
				System.Debug('******************** Vacacion antigua: ' + vacacionAntigua);
				listVacaciones.add(vacacionAntigua);
			}

			vacacionNueva.Dias_de_vacaciones__c = 22 + diasVacacionesNoUsados;
        	vacacionNueva.Anyo__c = Date.today().year();
        	vacacionNueva.Name='Vacaciones de ' + u.FirstName + ' ' + u.LastName +  ' ' + Date.today().year();
        	listVacaciones.add(vacacionNueva);
        }

        if(!listVacaciones.isEmpty()){
        	upsert listVacaciones;}

        System.Debug('Lista de vacaciones = ' + listVacaciones);
	}
}