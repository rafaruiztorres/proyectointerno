/**
 * Clase test para comprobación de correcto funcionamiento en inserción y modificación de registros worklog
 * 
 * @coverage TM_TaskModifyWorlog.trigger, TM_TaskModifyWorklogClass [90%]
 *
 * Fecha: 24/04/2018
 * Autor: Pablo de Andrés Rodríguez
 */
@isTest
public class TM_WorklogTriggerTest {

	public static void TM_WorklogTriggerTest(){
		Test.startTest();
		testWorklogs();
		Test.stopTest();
	}
	
	/**
	 * Prueba de cara a inserción de registros worklog
	 */
	@isTest static void testWorklogs() {

		List<ProyectoJIRA__c> listP = new List<ProyectoJIRA__c>();
		List<Tarea__c> listT = new List<Tarea__c>();
		List<Worklog__c> listW = new List<Worklog__c>();
		List<Sprint__c> listS = new List<Sprint__c>();

		
		ProyectoJIRA__c p = new ProyectoJIRA__c(
			Name = 'Proyecto - Test',
			OwnerId = UserInfo.getUserId(),
			Key__c = 'TPROY');
		listP.add(p);
		insert listP;

		Sprint__c s = new Sprint__c(
			Name='Sprint de prueba',
			ProyectoJIRA__c = listP[0].Id
		);
		listS.add(s);
		insert listS;

		Tarea__c t = new Tarea__c(
			Name = 'Tarea test worklogs',
			IdProyecto__c = listP[0].Id,
			Id__c = 'P' + listP[0].Id__c + '-' + '29560',
			Solicitante_Tarea__c = 'Pablo',
			Sprint__c = listS[0].Id,
			Key__c = 'PTPROY-9',
			Estimaci_n__c = 12.00);
		listT.add(t);
		insert listT;

		for(Integer i=0; i<3; i++){
			Datetime currentDT = Datetime.now();
			Worklog__c w = new Worklog__c(
				IdTarea__c = listT[0].Id,
				Creado__c = currentDT,
				Empezado__c = currentDT,
				Horas__c = 2.00,
				Consultor__c = UserInfo.getUserId()
			);
			listW.add(w);
		}
		insert listW;

		listW[0].Horas__c = 5.00;
		update listW[0];
	}
}