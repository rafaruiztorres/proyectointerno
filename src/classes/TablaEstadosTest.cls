@isTest
private class TablaEstadosTest {
	
	@isTest(seeAllData='true') static void testGetTareasyURL() {

		ProyectoJIRA__c pj = new ProyectoJIRA__c(
			Name = 'Prueba'
			);

		insert pj;

		Sprint__c sprint = new Sprint__c(
			Name='Sprint test',
			ProyectoJIRA__c = pj.Id
			);

		insert sprint;

		Tarea__c t1 = new Tarea__c(
			Name='Tarea prueba',
			IdProyecto__c = pj.Id,
			Sprint__c = sprint.Id,
			Estado__c = 'Pendiente'
			);

		insert t1;

		TablaEstados.getTareas('Pendiente');
		TablaEstados.getURLInforme('Pendiente');

	}

	
	

}