({
	horasPosiblesTotales : function(component) {
		var action = component.get('c.getHorasPosiblesTotales');
		var mes2 = component.find('select_meses').get('v.value');
		var anyo2 = component.find('select_anyos').get('v.value');
		action.setParams({ mes : mes2,anyo:anyo2,porIncurrir:false});
		action.setCallback(this,function(response){
		var state = response.getState();
		//console.log(state);
			if (state == 'SUCCESS'){
				component.set('v.horasPosiblesTotales',response.getReturnValue());
			}
		})
		$A.enqueueAction(action);
	},

	horasIncurridas : function(component) {

		var action2 = component.get('c.getHorasIncurridas');
		var mes2 = component.find('select_meses').get('v.value');
		var anyo2 = component.find('select_anyos').get('v.value');
		action2.setParams({mes:mes2,anyo:anyo2});
		action2.setCallback(this,function(response){
		var state = response.getState();
		//console.log(state);
			if (state == 'SUCCESS'){
				component.set('v.horasIncurridas',response.getReturnValue());
			}
		})
		$A.enqueueAction(action2);
		
	},

	horasPorIncurrir : function(component) {
		var action = component.get('c.getHorasPosiblesTotales');
		var mes2 = component.find('select_meses').get('v.value');
		var anyo2 = component.find('select_anyos').get('v.value');
		action.setParams({ mes : mes2,anyo:anyo2,porIncurrir:true});
		action.setCallback(this,function(response){
		var state = response.getState();
		//console.log(state);
			if (state == 'SUCCESS'){
				component.set('v.horasPorIncurrir',response.getReturnValue());
			}
		})
		$A.enqueueAction(action);
		
	}


})