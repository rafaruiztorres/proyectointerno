public with sharing class TablaDudas {

	@AuraEnabled
	public static integer getDudasQueTengoQueResolver(){
		String id_usuario = UserInfo.getUserId();
		Integer dudas = [SELECT count() 
						FROM Duda__c 
						WHERE Asignado_a__c =: id_usuario
						AND Estado__c != 'Resuelta'];

		return dudas;
	}

	@AuraEnabled
	public static integer getDudasQueMeTienenQueResolver(){
		String id_usuario = UserInfo.getUserId();
		Integer dudas = [SELECT count() 
						FROM Duda__c 
						WHERE OwnerId =: id_usuario
						AND Estado__c != 'Resuelta'];
		return dudas;
	}

	@AuraEnabled
	public static String getURLInformeDudasQueTengoQueResolver(){

		String baseURL = URL.getSalesforceBaseUrl().toExternalForm();
		String userId = UserInfo.getUserId();
		Report informe = [SELECT Id 
						 FROM Report
						 WHERE DeveloperName = 'Informe_de_DudasQueTengoQueResolver_FfR'];

		String linkInforme = baseURL + '/lightning/r/Report/' + informe.Id + '/view?fv0=' + userId ;
		return linkInforme;

	}

	@AuraEnabled
	public static String getURLInformeDudasQueMeTienenQueResolver(){
		String baseURL = URL.getSalesforceBaseUrl().toExternalForm();
		String userId = UserInfo.getUserId();
		Report informe = [SELECT Id 
						 FROM Report
						 WHERE DeveloperName = 'Informe_de_DudasQueMeTienenQueResolver_Iqp'];

		String linkInforme = baseURL + '/lightning/r/Report/' + informe.Id + '/view?fv0=' + userId ;
		return linkInforme;

	}

}