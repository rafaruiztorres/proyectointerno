/** 
 * <p>
 * TM_Workflows : Clase para el componente Lightning. Muestra las horas posibles de incurrir en un mes, las horas incurridas y las que 
 * se han podido incurrir a día de hoy en función del mes y del año.
 * </p>
 * Date: 21/9/2018	
 * @author ticMind Consulting (Rafa Ruiz - rafael.ruiz@ticmind.es)
 * @version 1.0
*/

public with sharing class TM_Workflows {

	/**
	*
	* Esta función nos saca las horas que se pueden trabajar en un mes.
	* @param mes,anyo Mes y año que obtenemos en el front.
	* @param porIncurrir Si porIncurrir es "true" significa que queremos sacar las horas que resten por trabajar en el mes. 
	* En caso de ser false, vamos a sacar todas las horas del mes.
	*
	* @return Devuelve el número de horas en formato decimal (0.5 = media hora).
	*/

	@AuraEnabled
	public static Decimal getHorasPosiblesTotales(Integer mes,Integer anyo,Boolean porIncurrir){
		
				
		/*
		System.Debug('@@@@ Por Incurrir @@@@' + porIncurrir);
		System.Debug('@@@@ Mes @@@@ ' + mes);
		System.Debug('@@@@ Año @@@@ ' + anyo); */
		
		mes = Integer.valueOf(mes);
		anyo = Integer.valueOf(anyo);

		Decimal numHorasTotales = 0;
		Integer diaFin;
		Decimal horasTrabajadas;

		if(porIncurrir==true){
			if(System.today().year() == anyo && System.today().month() == mes){
				diaFin = Date.today().day();	
			}else{
				diaFin =diasDelMes(mes,anyo);		
			}
			horasTrabajadas = getHorasIncurridas(mes,anyo);
		} else{
			diaFin = diasDelMes(mes,anyo);
			horasTrabajadas = 0;
		}

		//System.debug('Días en el mes: '+ diaFin);

		String diaSemana;
		Integer dia=1;
		Date fechaComparar = Date.newInstance(anyo,mes,diaFin);

		List<Configuracion_Anual__c> configuracionesAnuales = 
		[SELECT 
		 Id,
		 Name,
		 Fecha_de_inicio__c,
		 Fecha_de_fin__c,
		 Horas_semana__c,
		 Horas_viernes__c,
		 Horas_jornada_reducida__c,
		 Fecha_de_inicio_jornada_reducida__c,
		 Fecha_de_fin_jornada_reducida__c
		 FROM Configuracion_Anual__c
		 WHERE Fecha_de_inicio__c <= :fechaComparar
		 AND Fecha_de_fin__c >= :fechaComparar ];	

		System.debug('@@@@ Configuraciones iniciales: ' + configuracionesAnuales);

		/**
		Pasa por todos los días del mes, comprueba a que configuración corresponde ese día del mes.
		Luego, si no es sabado ni domingo, comprueba si es viernes o no para sumar las horas necesarias.
		Antes de eso, comprobará si estamos o no en jornada reducida.
		*/

		for (dia=1;dia<=diaFin;dia++){
			Date fechaElegirConfig = Date.newInstance(anyo,mes,dia);
			for(Integer i=0; i<=configuracionesAnuales.size()-1;i++){
				Configuracion_Anual__c configuracionAnual = configuracionesAnuales.get(i);
				if ((configuracionAnual.Fecha_de_inicio__c <= fechaElegirConfig) && (fechaElegirConfig <= configuracionAnual.Fecha_de_fin__c)){
					if(!esFestivo(configuracionAnual,fechaElegirConfig)){
						diaSemana = getDiaSemana(fechaElegirConfig);
						System.debug('Fecha : ' + fechaElegirConfig + ' Es el día : ' + diaSemana);
						
						if(diaSemana!='Sunday' && diaSemana!='Saturday'){

							if((configuracionAnual.Fecha_de_inicio_jornada_reducida__c <= fechaElegirConfig) && (fechaElegirConfig <= configuracionAnual.Fecha_de_fin_jornada_reducida__c)){
								numHorasTotales += configuracionAnual.Horas_jornada_reducida__c;
								System.debug('Num de horas totales = ' + numHorasTotales);
							}
							else{
								if(diaSemana!='Friday'){
									numHorasTotales += configuracionAnual.Horas_semana__c;
									System.debug('Num de horas totales =' + numHorasTotales);
								}
								else{
									numHorasTotales += configuracionAnual.Horas_viernes__c;
									System.debug('Num de horas totales (else) = ' + numHorasTotales);
								}
							}
							
						}
						
					}			
				}
			}													  
		}

		//System.debug('Num de horas totales  = ' + numHorasTotales);
		return numHorasTotales - horasTrabajadas;
	}

	/**
	* Saca las horas que has trabajado en función del mes y el año.
	* @param mes Mes pasado por parámetro
	* @param anyo Año pasado por parámetro
	* 
	* @return Devuelve las horas incurridas en formato Decimal. 
	*/

	@AuraEnabled
	public static Decimal getHorasIncurridas(Integer mes,Integer anyo){
		
		mes = Integer.valueOf(mes);
		anyo = Integer.valueOf(anyo);

		String idUsuario = UserInfo.getUserId();
		Date diaFin;
		Decimal horasIncurridas = 0;

		if(System.today().year() == anyo && System.today().month() == mes){
			diaFin = Date.today();	
		}else{
			diaFin = Date.newInstance(anyo,mes,diasDelMes(mes,anyo));		
		}

		Date diaInicio = Date.newInstance(anyo,mes,1);

		//System.Debug('@@@@@ Dia de FIN : ' + diaFin);
		//System.Debug('@@@@@ Dia Inicio : ' + diaInicio);



		List<AggregateResult> sumHoras = [SELECT sum(Horas__c)horas 
						FROM Worklog__c	
						WHERE Consultor__c = :idUsuario
						AND Empezado__c >= :diaInicio
						AND Empezado__c <= :diaFin ];	
		//System.Debug('@@@@ sumHoras = ' + sumHoras);
		//System.Debug('@@@@ SELECT sum(Horas__c)horas FROM Worklog__c WHERE Consultor__c = ' + idUsuario + 'AND Empezado__c >= ' + diaInicio + 'AND Empezado__c <=' + diaFin);
		if(sumHoras[0].get('horas')!=null){
			horasIncurridas = (Decimal)sumHoras[0].get('horas');
		}
		return horasIncurridas;
	}

	/**
	 * Devuelve el número de días que tiene el mes.
	 *
	 * @param mes Mes del que queremos saber los días.
	 * @param anyo Año utilizado como parámetro (Necesario para saber si Febrero son 28 o 29 días).
	 * @return Devuelve un Integer con el numero de días del mes.
	*/

	public static Integer diasDelMes(Integer mes,Integer anyo){
		Integer numDias = date.daysInMonth(anyo,mes);
		return numDias;
	}

	/**
	 * Devuelve un string con la fecha de la semana.
	 *
	 * @param fecha Fecha que queremos saber si es festivo.
	 * @param configuracionAnual Incluye la configuración anual para la que comprobamos si un día es festivo o no.
	 * @return Devuelve true si es festivo, false si no lo es.
	*/
	public static Boolean esFestivo(Configuracion_Anual__c configuracionAnual,Date fecha){

		Boolean festivo = false;
		Integer hayFestivos = [SELECT count() FROM Festivos__c
							WHERE Configuracion_Anual__c = :configuracionAnual.Id
							AND Fecha__c = :fecha];

		if(hayFestivos>0){
			festivo = true;
		}
		return festivo;
	}

	/**
	 * Devuelve un string con la fecha de la semana.
	 *
	 * @param fecha Variable tipo "Date" que contiene una fecha.
	 * @return Devuelve una string con el día de la semana (Monday,Tuesday...)
	*/

	public static String getDiaSemana(Date fecha){
		Datetime dt = DateTime.newInstance(fecha.year(),fecha.month(),fecha.day());
		return dt.format('EEEE');
	}



}