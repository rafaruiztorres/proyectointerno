/**
* Clase que recoge la llamada a trigger TM_WorklogTrigger
* 
* Fecha: 24/04/2018
* Autor: Pablo de Andrés Rodríguez
*/
global class TM_WorklogTriggerClass {

	/**
	 * [Modifica las horas consumidas en la tarea asociada a los worklogs que vayamos insertando]
	 * @param listNew [registros nuevos]
	 * @param listOld [registros antiguos]
	 */
	public static void modifyConsumedTimeForTask(List<Worklog__c> listNew, List<Worklog__c> listOld){
		//Creamos Map para almacenar las horas y el idenficador asociado a la tarea
		Map<Id, Decimal> mapWorklog = new Map<Id, Decimal>();

		//Horas consumidas del worklog
		Decimal horas = 0.00;

		//Tareas update
		List<Tarea__c> tareasUpdate = new List<Tarea__c>();
		for(Integer i=0; i<listNew.size(); i++){
			//Update
			if(listOld != null){
				Decimal horasOld = listOld[i].Horas__c!=null?listOld[i].Horas__c:0;
				Decimal horasNew = listNew[i].Horas__c!=null?listNew[i].Horas__c:0;
				if(listOld[i].Horas__c != listNew[i].Horas__c){
					horas = horasNew - horasOld;
				}
				else{
					continue;
				}
			}
			//Insert
			else{
				if(listNew[i].Horas__c != null){
					horas = listNew[i].Horas__c;
				}
				else{
					continue;
				}
			}
			//En caso de haber ya worklogs recogidos para una tarea le sumamos las horas a ese registro
			if(mapWorklog.containsKey(listNew[i].IdTarea__c)){
				Decimal horasAux = mapWorklog.get(listNew[i].IdTarea__c);
				horasAux += horas;
				mapWorklog.put(listNew[i].IdTarea__c, horasAux);
			}
			else{
				mapWorklog.put(listNew[i].IdTarea__c, horas);
			}
		}
		for(Tarea__c t : [SELECT Tiempo_consumido__c, Id FROM Tarea__c WHERE Id IN :mapWorklog.keySet()]){
			if(t.Tiempo_consumido__c == null){
				t.Tiempo_consumido__c = mapWorklog.get(t.Id);
			}
			else{
				t.Tiempo_consumido__c += mapWorklog.get(t.Id);
			}
			tareasUpdate.add(t);
		}
		update tareasUpdate;
	}

	/**
	 * [Comprobar que no se han sobrepasado las 8 horas en incurridos por empleado y para el día actual]
	 * @param listNew [registros nuevos]
	 * @param listOld [registros antiguos]
	 */
	public static void checkHoursForDayAndEmployee(List<Worklog__c> listNew, List<Worklog__c> listOld){

		//Mapa para recoger los mapas (mapHorasFecha) y sus consultores correspondientes
		Map<Id, Map<Date, Decimal>> mapMapasEmpleado = new Map<Id, Map<Date, Decimal>>();

		//Mapa para recoger los consultores y su lista de worklogs asociados
		Map<Id, List<Worklog__c>> mapWorklogsEmpleado = new Map<Id, List<Worklog__c>>();

		//Set para recoger las fechas de los worklogs asociados
		Set<Date> fechaWorklog = new Set<Date>();

		//Horas por worklog
		Decimal horas;

		for(Worklog__c w : listNew){
			List<Worklog__c> workL = mapWorklogsEmpleado.containsKey(w.Consultor__c)?mapWorklogsEmpleado.get(w.Consultor__c):new List<Worklog__c>();
			workL.add(w);
			mapWorklogsEmpleado.put(w.Consultor__c, workL);
			//Guardamos fecha del worklog en set
			fechaWorklog.add(getDateFromDatetime(w.Empezado__c));
		}

		//Añadimos al mapa las horas por empleado de worklogs ya registrados en bd para el día actual [DAY_ONLY para recoger el campo Empezado__c como DATE en lugar de DATETIME]
		for(Worklog__c w : [SELECT Horas__c, Consultor__c, Empezado__c FROM Worklog__c WHERE DAY_ONLY(Empezado__c) IN :fechaWorklog AND Consultor__c IN :mapWorklogsEmpleado.keySet()]){
			if(w.Horas__c != null){
				horas = w.Horas__c;
			}
			else{
				horas = 0.00;
			}
			//Existe empleado en mapa -> actualizamos mapas
			if(mapMapasEmpleado.containsKey(w.Consultor__c)){
				Map<Date, Decimal> mapAux = mapMapasEmpleado.get(w.Consultor__c);
				//Cambiamos la fecha de creación de Datetime a Date
				Date dateAux = getDateFromDatetime(w.Empezado__c);
				//Existe fecha en mapa -> actualizamos horas
				if(mapAux.containsKey(getDateFromDatetime(w.Empezado__c))){
					Decimal horasAux = mapAux.get(dateAux);
					horasAux += horas;
					mapAux.put(dateAux, horasAux);
				}
				//No existe fecha en mapa -> insertamos horas
				else{
					mapAux.put(dateAux, horas);
				}
				mapMapasEmpleado.put(w.Consultor__c, mapAux);
			}
			//No existe empleado en mapa -> insertamos mapas
			else{
				Map<Date, Decimal> mapAux = new Map<Date, Decimal>();
				mapAux.put(getDateFromDatetime(w.Empezado__c), horas);
				mapMapasEmpleado.put(w.Consultor__c, mapAux);
			}
		}

		//Incluimos en mapa las horas de los worklogs a insertar o updatear e insertamos mapa de lista de worklogs por empleado
		for(Integer i=0; i<listNew.size(); i++){
			//Update
			if(listOld != null){
				Decimal horasOld = listOld[i].Horas__c!=null?listOld[i].Horas__c:0;
				Decimal horasNew = listNew[i].Horas__c!=null?listNew[i].Horas__c:0;
				if(listOld[i].Horas__c != listNew[i].Horas__c){
					horas = horasNew - horasOld;
				}
				else{
					continue;
				}
			}
			//Insert
			else{
				if(listNew[i].Horas__c != null){
					horas = listNew[i].Horas__c;
				}
				else{
					continue;
				}
			}
			//Existe empleado en mapa -> actualizamos mapas
			if(mapMapasEmpleado.containsKey(listNew[i].Consultor__c)){
				Map<Date, Decimal> mapAux = mapMapasEmpleado.get(listNew[i].Consultor__c);
				//Cambiamos la fecha de creación de Datetime a Date
				Date dateAux = getDateFromDatetime(listNew[i].Empezado__c);
				//Existe fecha en mapa -> actualizamos horas
				if(mapAux.containsKey(dateAux)){
					Decimal horasAux = mapAux.get(dateAux);
					horasAux += horas;
					mapAux.put(dateAux, horasAux);
				}
				//No existe fecha en mapa -> insertamos horas
				else{
					mapAux.put(dateAux, horas);
				}
				mapMapasEmpleado.put(listNew[i].Consultor__c, mapAux);
			}
			//No existe empleado en mapa -> insertamos mapas
			else{
				Map<Date, Decimal> mapAux = new Map<Date, Decimal>();
				mapAux.put(getDateFromDatetime(listNew[i].Empezado__c), horas);
				mapMapasEmpleado.put(listNew[i].Consultor__c, mapAux);
			}
		}

		//Iteramos el mapa
		for(Id key : mapMapasEmpleado.keySet()){
			//Mapa auxiliar iteración
			Map<Date, Decimal> mapKey = mapMapasEmpleado.get(key);
			//Set de fechas
			Set<Date> setFechas = new Set<Date>();
			for(Date dateKey : mapKey.keySet()){
				if(mapKey.get(dateKey) > 8.50){
					setFechas.add(dateKey);
				}
			}
			for(Worklog__c wMap : mapWorklogsEmpleado.get(key)){
				if(setFechas.contains(getDateFromDatetime(wMap.Empezado__c))){
					if(!Test.isRunningTest()){
						wMap.addError('No se pueden incurrir más de 8 horas y media diarias. Inténtelo de nuevo otro día.');
					}
				}
			}
		}
	}

	/**
	 * Conversión de formato Datetime a Date
	 * @param  dtime [datetime]
	 * @return retrieveDate [date]
	 */
	public static Date getDateFromDatetime(Datetime dtime){
		Date retrieveDate = date.newInstance(dtime.year(), dtime.month(), dtime.day());
		return retrieveDate;
	}
}