/**
* Trigger para controlar las modificaciones o inserciones de registros worklog
* Fecha: 24/04/2018
* Autor: Pablo de Andrés Rodríguez
*/
trigger TM_WorklogTrigger on Worklog__c (after insert, after update, before insert, before update) {
	if(trigger.isAfter){
		if(trigger.isInsert){
			TM_WorklogTriggerClass.modifyConsumedTimeForTask(trigger.new, null);
		}
		if(trigger.isUpdate){
			TM_WorklogTriggerClass.modifyConsumedTimeForTask(trigger.new, trigger.old);
		}
	}
	if(trigger.isBefore){
		if(trigger.isInsert){
			TM_WorklogTriggerClass.checkHoursForDayAndEmployee(trigger.new, null);
		}
		if(trigger.isUpdate){
			TM_WorklogTriggerClass.checkHoursForDayAndEmployee(trigger.new, trigger.old);
		}
	}
}