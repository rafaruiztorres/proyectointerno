public class Vacaciones {

    public static Integer getDiasVacaciones(Date fechaIncorporacion){
        Integer diasVacaciones=22;
		Decimal vacacionesPorDia = Decimal.valueOf(diasVacaciones).divide(365,7);
        
		Date anyoActual = Date.newInstance(Date.today().year(),12,31);
        
 		if(fechaIncorporacion.year()==anyoActual.year()){
  			diasVacaciones = Integer.valueof((fechaIncorporacion.daysBetween(anyoActual)
                              *vacacionesPorDia).round(System.RoundingMode.HALF_DOWN));
  		 }

        return diasVacaciones;
        
    }
    
    public static void eliminaVacacionesSiTiene(User usuario){
        
        Vacaciones__c v = [ SELECT Id FROM Vacaciones__c WHERE Empleado__c =:usuario.Id ];
        if (v.Id!=null){
            delete v;
        }
    }
}