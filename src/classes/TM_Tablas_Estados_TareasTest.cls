/** 
 * Test Class for TM_Tablas_Estados_Tareas
 * Date: 21/9/2018
 * @author Rafa Ruiz (rafael.ruiz@ticmind.es)
 * @version 1.0
 * @Coverage: 100%
*/
@isTest
private class TM_Tablas_Estados_TareasTest {
	
	@isTest(seeAllData='true') static void testGetTareasyURL() {

		ProyectoJIRA__c pj = new ProyectoJIRA__c(
			Name = 'Prueba'
			);

		insert pj;

		Sprint__c sprint = new Sprint__c(
			Name='Sprint test',
			ProyectoJIRA__c = pj.Id
			);

		insert sprint;

		Tarea__c t1 = new Tarea__c(
			Name='Tarea prueba',
			IdProyecto__c = pj.Id,
			Sprint__c = sprint.Id,
			Estado__c = 'Pendiente'
			);

		insert t1;

		TM_Tablas_Estados_Tareas.getTareas('Pendiente');
		TM_Tablas_Estados_Tareas.getURLInforme('Pendiente');

	}

}