trigger TM_UserTriggerFechaIncorporacion on User (after insert,after update) {
    
    if (Trigger.isInsert){
        TM_UserTriggerFechaIncorporacionClass.crearVacaciones(trigger.new,null);
    }

    if (Trigger.isUpdate){
    	TM_UserTriggerFechaIncorporacionClass.actualizarCrearVacaciones(trigger.new,trigger.old);
    }
}