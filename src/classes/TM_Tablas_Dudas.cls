/** 
 * <p>
 * TM_Tablas_Dudas : Clase para el componente Lightning. Muestra el número de dudas que tengo y me tienen que resolver.
 * y enlaza a ellas mediante un informe.
 * </p>
 * Date: 21/9/2018	
 * @author ticMind Consulting (Rafa Ruiz - rafael.ruiz@ticmind.es)
 * @version 1.0
*/


public with sharing class TM_Tablas_Dudas {

	@AuraEnabled
	public static integer getDudasQueTengoQueResolver(){
		String id_usuario = UserInfo.getUserId();
		Integer dudas = [SELECT count() 
						FROM Duda__c 
						WHERE Asignado_a__c =: id_usuario
						AND Estado__c != 'Resuelta'];

		return dudas;
	}

	@AuraEnabled
	public static integer getDudasQueMeTienenQueResolver(){
		String id_usuario = UserInfo.getUserId();
		Integer dudas = [SELECT count() 
						FROM Duda__c 
						WHERE OwnerId =: id_usuario
						AND Estado__c != 'Resuelta'];
		return dudas;
	}

	@AuraEnabled
	public static String getURLInformeDudasQueTengoQueResolver(){

		String baseURL = URL.getSalesforceBaseUrl().toExternalForm();
		String userId = UserInfo.getUserId();
		Report informe = [SELECT Id 
						 FROM Report
						 WHERE DeveloperName = 'Informe_de_DudasQueTengoQueResolver_FfR'];

		String linkInforme = baseURL + '/lightning/r/Report/' + informe.Id + '/view?fv0=' + userId ;
		return linkInforme;

	}

	@AuraEnabled
	public static String getURLInformeDudasQueMeTienenQueResolver(){
		String baseURL = URL.getSalesforceBaseUrl().toExternalForm();
		String userId = UserInfo.getUserId();
		Report informe = [SELECT Id 
						 FROM Report
						 WHERE DeveloperName = 'Informe_de_DudasQueMeTienenQueResolver_Iqp'];

		String linkInforme = baseURL + '/lightning/r/Report/' + informe.Id + '/view?fv0=' + userId ;
		return linkInforme;

	}

}