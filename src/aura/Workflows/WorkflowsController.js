({
	init : function(component, event, helper) {
	    var d = new Date();
		var month = d.getMonth()+1;
		var year = d.getFullYear();
		var valorSelect = component.find("select_meses").set("v.value",month);
		var valorAnyos = component.find("select_anyos").set("v.value",year);

		for (var i=0;i<=4;i++){
			component.set('v.array_anyos['+ i + ']',year);
			year--;
		}

		helper.horasPosiblesTotales(component);
		helper.horasIncurridas(component);
		helper.horasPorIncurrir(component);
	},

	handleClick : function(component,event,helper)
	{
		helper.horasPosiblesTotales(component);
		helper.horasIncurridas(component);
		helper.horasPorIncurrir(component);
	}
})