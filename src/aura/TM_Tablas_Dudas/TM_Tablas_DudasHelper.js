({
	loadCountDudas : function(component) {
		
		var action = component.get("c.getDudasQueTengoQueResolver");
		action.setCallback(this,function(response){
			var state = response.getState();
			if (state == 'SUCCESS'){
				component.set('v.dudasQueTengoQueResolver',response.getReturnValue());
			}
		})
		$A.enqueueAction(action);

		var action2 = component.get("c.getDudasQueMeTienenQueResolver");
		action2.setCallback(this,function(response){
			var state = response.getState();
			if (state == 'SUCCESS'){
				component.set('v.dudasQueMeTienenQueResolver',response.getReturnValue());
			}
		})
		$A.enqueueAction(action2);
	},

	asignarURLs : function(component){
		
		var action = component.get("c.getURLInformeDudasQueTengoQueResolver");
		action.setCallback(this,function(response){
			var state = response.getState();
			if (state == 'SUCCESS'){
				component.set("v.urlDudasQueTengoQueResolver",response.getReturnValue());
			}
		})
		$A.enqueueAction(action);

		var action2 = component.get("c.getURLInformeDudasQueMeTienenQueResolver");
		action2.setCallback(this,function(response){
			var state = response.getState();
			if (state == 'SUCCESS'){
				component.set("v.urlDudasQueMeTienenQueResolver",response.getReturnValue());
			}
		})
		$A.enqueueAction(action2);
	}

})