({

	loadCountTareas : function(component){
		
		var action = component.get("c.getTareas");
		action.setParams({ estado : "Pendiente" });
		action.setCallback(this,function(response){
			var state = response.getState();
			if (state == 'SUCCESS'){
				component.set("v.tareas_pendientes",response.getReturnValue());
			}
		})
		$A.enqueueAction(action);

		var action2 = component.get("c.getTareas");
		action2.setParams({ estado : "Preparada" });
		action2.setCallback(this,function(response){
			var state = response.getState();
			if (state == 'SUCCESS'){
				component.set("v.tareas_preparadas",response.getReturnValue());
			}
		})
		$A.enqueueAction(action2);

		var action3 = component.get("c.getTareas");
		action3.setParams({ estado : "En curso" });
		action3.setCallback(this,function(response){
			var state = response.getState();
			if (state == 'SUCCESS'){
				component.set("v.tareas_en_curso",response.getReturnValue());
			}
		})
        $A.enqueueAction(action3);

        var action4 = component.get("c.getURLInforme");
        action4.setParams({ estado : "Pendiente" });
        action4.setCallback(this,function(response){
        	var state = response.getState();
        	if (state == 'SUCCESS'){
        	component.set("v.url_pendientes",response.getReturnValue());
        	}
        })
        $A.enqueueAction(action4);

        var action5 = component.get("c.getURLInforme");
        action5.setParams({ estado : "Preparada" });
        action5.setCallback(this,function(response){
        	var state = response.getState();
        	if (state == 'SUCCESS'){
        	component.set("v.url_preparadas",response.getReturnValue());
        	}
        })
        $A.enqueueAction(action5);

        var action6 = component.get("c.getURLInforme");
       	action6.setParams({ estado : "En curso" });
        action6.setCallback(this,function(response){
        	var state = response.getState();
        	if (state == 'SUCCESS'){
        	component.set("v.url_en_curso",response.getReturnValue());
        	}
        })
        $A.enqueueAction(action6);
      

	 }

})