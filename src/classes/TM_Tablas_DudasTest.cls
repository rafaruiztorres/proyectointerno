/** 
 * Test Class for TM_Tablas_Dudas
 * Date: 21/9/2018
 * @author Rafa Ruiz (rafael.ruiz@ticmind.es)
 * @version 1.0
 * @Coverage: 100%
*/
@isTest
private class TM_Tablas_DudasTest {
	
	@isTest(seeAllData='true') static void getDudasEInformes() {
		ProyectoJIRA__c pj = new ProyectoJIRA__c(
			Name = 'Prueba'
			);

		insert pj;

		Sprint__c sprint = new Sprint__c(
			Name='Sprint test',
			ProyectoJIRA__c = pj.Id
			);

		insert sprint;

		Tarea__c t1 = new Tarea__c(
			Name='Tarea prueba',
			IdProyecto__c = pj.Id,
			Sprint__c = sprint.Id
			);

		insert t1;

		Duda__c duda = new Duda__c(
			Asignado_a__c = UserInfo.getUserId(),
			Estado__c = 'Pendiente',
			Name = 'Duda test',
			Tarea__c = t1.Id
			);

		insert duda;

		TM_Tablas_Dudas.getDudasQueTengoQueresolver();
		TM_Tablas_Dudas.getDudasQueMeTienenQueResolver();
		TM_Tablas_Dudas.getURLInformeDudasQueTengoQueResolver();
		TM_Tablas_Dudas.getURLInformeDudasQueMeTienenQueResolver();
	}
	
	
}