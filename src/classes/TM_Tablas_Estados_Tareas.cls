/** 
 * <p>
 * TM_Tablas_Estados_Tareas : Clase para el componente Lightning. Muestra el número de tareas que tengo, su estado
 * y enlaza a ellas mediante un informe.
 * </p>
 * Date: 21/9/2018	
 * @author ticMind Consulting (Rafa Ruiz - rafael.ruiz@ticmind.es)
 * @version 1.0
*/

public with sharing class TM_Tablas_Estados_Tareas {
    @AuraEnabled
	public static Integer getTareas(String estado)
	{	
		String id_usuario = UserInfo.getUserId();
		Integer tareas = [SELECT Count()
								FROM Tarea__c
								WHERE OwnerId =:id_usuario
								AND Estado__c =:estado];
		return tareas;
	}

	//Vamos a sacar la URL del informe para que acceda al pinchar en el número.
	@AuraEnabled
	public static String getURLInforme(String estado){

		String baseURL = URL.getSalesforceBaseUrl().toExternalForm();
		String userId = UserInfo.getUserId();
		Report informe = [SELECT Name,Id
					FROM Report
					WHERE DeveloperName = 'Informe_tareas_estado_usuario_7OH'];

		String linkInforme = baseURL + '/lightning/r/Report/' + informe.Id + '/view?fv0=' + userId + '&fv1=' + estado;

		return linkInforme;

	}

}