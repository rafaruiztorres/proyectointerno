@isTest
private class creaRegistroVacacionesNuevoAnyoTest {
	
	//Fecha CRON Dummy
	public static String CRON_EXP = '0 0 0 15 3 ? 2022';
	@isTest static void testcreaRegistro() {

		Vacaciones__c vacaciones = new Vacaciones__c(
			Activo__c = true,
			Anyo__c = 2017,
			Dias_de_vacaciones__c = 22,
			Dias_consumidos__c = 20,
			Empleado__c = UserInfo.getUserId(),
			Name = 'Vacaciones TEST');

		insert vacaciones;
		Test.startTest();
		String jobId = System.schedule('ScheduledApexTest',
            CRON_EXP, 
            new creaRegistroVacacionesNuevoAnyo());        
        Test.stopTest();

	}
	
}