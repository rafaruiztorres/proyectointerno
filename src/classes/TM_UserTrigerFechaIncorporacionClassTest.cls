@isTest
private class TM_UserTrigerFechaIncorporacionClassTest {

	@isTest
	private static void testTriggerCrearActualizarVacaciones() {

		Profile profileId = [SELECT Id FROM Profile WHERE Name = 'Administrador' LIMIT 1];

		Date d = Date.newInstance(2017, 09, 01);
		Date d2 = Date.newInstance(2018, 09, 01);

		User usr = new User(LastName = 'LIVESTON22',
		                    FirstName = 'JASON22',
		                    Alias = 'jli2v',
		                    Email = 'jason22.liveston@asdf.com',
		                    Username = 'jason22.liveston@asdf.com',
		                    ProfileId = profileId.id,
		                    TimeZoneSidKey = 'GMT',
		                    LanguageLocaleKey = 'en_US',
		                    EmailEncodingKey = 'UTF-8',
		                    LocaleSidKey = 'en_US',
		                    Fecha_de_incorporacion__c = d
		);

		User usr2 = new User(LastName = 'LIVE2STON22',
		                     FirstName = 'JA2SON22',
		                     Alias = 'jli22v',
		                     Email = 'jason222.liveston@asdf.com',
		                     Username = 'jason222.liveston@asdf.com',
		                     ProfileId = profileId.id,
		                     TimeZoneSidKey = 'GMT',
		                     LanguageLocaleKey = 'en_US',
		                     EmailEncodingKey = 'UTF-8',
		                     LocaleSidKey = 'en_US'		);

		insert usr;
		insert usr2;

		Vacaciones__c v = new Vacaciones__c(
		                                    Activo__c = true,
		                                    anyo__c = 2018,
		                                    Dias_consumidos__c = 0,
		                                    Empleado__c = usr2.Id
		);

		insert v;

		usr.Lastname = 'LIVESTON222';

		update usr;
		
		usr2.Fecha_de_incorporacion__c = d2;
		update usr2;

	}
}