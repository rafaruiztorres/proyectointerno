public without sharing class TM_TareaTriggerClass  {

	/* public static void setRecursoTarea(List<Tarea__c> lNew,List<Tarea__c> lOld){
		
		//Si estamos insertando una nueva tarea.
		List<User> lUser = new List<User>();
		Set<Id> sIdsTareas = new Set<Id>();
		Set<Id> sIdsOwner = new Set<Id>();
		Map<Id,String> mIdTipoRecurso = new Map<Id,String>();
		System.debug('lNew' + lNew);
		for (Tarea__c t: lNew){
			sIdsTareas.add(t.Id);
		}

		// Lista con relaciones
		//List<Tarea__c> lNewR = [SELECT OwnerId FROM Tarea__c WHERE Id in :sIdsTareas];

		for (Tarea__c tOwner : [SELECT OwnerId FROM Tarea__c WHERE Id in :sIdsTareas]){
			sIdsOwner.add(tOwner.OwnerId);	
		}

		for (User u : [SELECT Id,Tipo_de_recurso__c FROM User WHERE Id in :sIdsOwner]){
			mIdTipoRecurso.put(u.Id,u.Tipo_de_recurso__c);
		}

		System.debug('Sidstareas: ' + sIdsTareas); b
		System.debug('sIdsOwner: ' + sIdsOwner);
		System.debug('mIdTipoRecurso' + mIdTipoRecurso);

		if(lOld==null){
			for (Tarea__c t : lNew){
				if(String.isNotEmpty(mIdTipoRecurso.get(t.OwnerId))){
					t.Tipo_de_recurso__c = mIdTipoRecurso.get(t.OwnerId);
				}
			}
		}else{
			for(Integer i=0;i<lNew.size();i++){
				if(lNew[i].OwnerId!=lOld[i].OwnerId){
					for (Tarea__c t : lNew){
						if(String.isNotEmpty(mIdTipoRecurso.get(t.OwnerId))){
							t.Tipo_de_recurso__c = mIdTipoRecurso.get(t.OwnerId);
						}				
					}	
				}
			}	
		}
	}	

	*/

	public static void setRecursoTarea(List<Tarea__c> lNew,List<Tarea__c> lOld){
		Set<String> sIdUsersToSearch = new Set<String>();
		Map<Id,User> mIdUserAndUser = new Map<Id,User>();

		for(Tarea__c tarea: lNew){ sIdUsersToSearch.add(tarea.OwnerId); }
		
		if(lOld != null ){ for(Tarea__c tarea: lOld){ sIdUsersToSearch.add(tarea.OwnerId); } }

		for(User usuario : [select Id, 
								   Tipo_de_recurso__c 
							from User 
							where Id in : sIdUsersToSearch]){ mIdUserAndUser.put(usuario.Id,usuario); }
		
		// Actualizamos el tipo de recurso de la tarea segun su owner.
		for (Integer i = 0; i< lNew.size(); i++){
			if(lOld != null && !lOld.isEmpty() && lNew[i].OwnerId != lOld[i].OwnerId){
				lNew[i].Tipo_de_recurso__c = mIdUserAndUser.containsKey(lNew[i].OwnerId) ? mIdUserAndUser.get(lNew[i].OwnerId).Tipo_de_recurso__c : null;
			}else{
				lNew[i].Tipo_de_recurso__c = mIdUserAndUser.containsKey(lNew[i].OwnerId)? mIdUserAndUser.get(lNew[i].OwnerId).Tipo_de_recurso__c : null;
			}
		}

		System.debug('@@@@ lNew:' + JSON.serialize(lNew));
	}
}