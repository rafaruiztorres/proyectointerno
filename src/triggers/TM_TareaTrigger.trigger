trigger TM_TareaTrigger on Tarea__c (before insert, before update)  {
    if(Trigger.isBefore){
		if (Trigger.isInsert){
			TM_TareaTriggerClass.setRecursoTarea(trigger.new,null);
		}

		if (Trigger.isUpdate){
    		TM_TareaTriggerClass.setRecursoTarea(trigger.new,trigger.old);
		}
	}
}