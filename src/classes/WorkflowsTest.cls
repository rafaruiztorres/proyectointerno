@isTest
private class WorkflowsTest {
	
	@isTest static void testHorasPosiblesTotales() {
		
		DateTime dt0 = DateTime.newInstance(2018,9,10);
		DateTime dt1 = Datetime.newInstance(2018,9,29);
		Datetime dtwl = Datetime.newInstance(2018,9,13);
		
		/*-----------------------*/

		Date fecIniConfig = Date.newInstance(2018,1,1);
		Date fecFinConfig = Date.newInstance(2018,12,31);
		Date fecIniJornadaRed = Date.newInstance(2018,7,15);
		Date fecFinJornadaRed = Date.newInstance(2018,9,15);

		Date diaFestivo = Date.newInstance(2018,10,11);

		Configuracion_Anual__c configAnual = new Configuracion_Anual__c(
			Fecha_de_inicio__c = fecIniConfig,
			Fecha_de_fin__c = fecFinConfig,
			Fecha_de_inicio_jornada_reducida__c = fecIniJornadaRed,
			Fecha_de_fin_jornada_reducida__c = fecFinJornadaRed,
			Horas_jornada_reducida__c = 7,
			Horas_semana__c = 8.5,
			Horas_viernes__c = 6.5,
			Name = 'Configuracion de test'
			);

		insert configAnual;

		Festivos__c festivo = new Festivos__c(
			Fecha__c = diaFestivo,
			Configuracion_Anual__c = configAnual.Id,
			Name = 'Dia festivo test'
			);

		insert festivo;
		
		ProyectoJIRA__c pj = new ProyectoJIRA__c(
			Name = 'Prueba'
			);

		insert pj;

		Sprint__c sprint = new Sprint__c(
			Name='Sprint test',
			ProyectoJIRA__c = pj.Id
			);

		insert sprint;

		Tarea__c t1 = new Tarea__c(
			Name='Tarea prueba',
			IdProyecto__c = pj.Id,
			Sprint__c = sprint.Id
			);

		insert t1;

		Worklog__c wl = new Worklog__c(
			Empezado__c = dtwl,
			Horas__c = 4,
			Consultor__c = UserInfo.getUserId(),
			IdTarea__c = t1.Id
			);

		insert wl;

		Decimal horas1 = Workflows.getHorasPosiblesTotales(9,2018,false);
		System.assertEquals(151,horas1);
		Decimal horas2 = Workflows.getHorasPosiblesTotales(9,2018,true);
		System.assertEquals(83,horas2);
		Decimal horas3 = Workflows.getHorasPosiblesTotales(10,2018,true);
		System.assertEquals(179,horas3);
	}


}