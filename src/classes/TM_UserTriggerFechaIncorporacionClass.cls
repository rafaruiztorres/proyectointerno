/**
* Clase que recoge la llamada a trigger TM_UserTriggerFechaIncorporacion
* 
* Fecha: 19/09/2018
* Autor: Rafa Ruiz
*/
global class TM_UserTriggerFechaIncorporacionClass {
	
	/**
	La siguiente función crea un objeto vacaciones asociados al usuario cuando este se crea.
	*/
	public static void crearVacaciones(List<User> listNew, List<User> listOld){
		List<Vacaciones__c> list_v = new List <Vacaciones__c>();
		Vacaciones__c v;
        for(User u: listNew) {
            if(u.Fecha_de_incorporacion__c!=null){
				v = new Vacaciones__c(
                	Dias_de_vacaciones__c = getDiasVacaciones(u.Fecha_de_incorporacion__c),
                    Empleado__c = u.Id,
                    Name = 'Vacaciones de ' + u.FirstName + ' ' + u.LastName + ' ' + Date.today().year(),
                    Activo__c = true
            	);   
            	list_v.add(v);
            }
        }
        insert list_v;
	}

	/**
	La siguiente función crea una lista de vacaciones, busca.
	*/
	public static void actualizarCrearVacaciones(List<User> listNew,List<User> listOld){
		List<Vacaciones__c> list_v = new List<Vacaciones__c>();
		Set<Id> sIds = new Set<Id>();
		Map<Id, Vacaciones__c> mIdUserVacaciones = new Map<Id, Vacaciones__c>();

		// Recorremos los usuarios para poder hacer la consulta bulk
		for(User usuario : listNew){
			sIds.add(usuario.Id);
		}
		
		// Obtenemos los registros de vacaciones de cada usuario
		for(Vacaciones__c vacacion : [ SELECT Id,
		 								Empleado__c,
		 								Empleado__r.Fecha_de_incorporacion__c,
		 								Empleado__r.FirstName,
		 								Empleado__r.LastName 
		 								FROM Vacaciones__c 
		 								WHERE Empleado__c in :sIds AND anyo__c = :Date.today().year() limit 1]){
        	vacacion.Dias_de_vacaciones__c = getDiasVacaciones(vacacion.Empleado__r.Fecha_de_incorporacion__c);
        	vacacion.Anyo__c = Date.today().year();
        	vacacion.Name='Vacaciones de ' + vacacion.Empleado__r.FirstName + ' ' + vacacion.Empleado__r.LastName + ' ' + Date.today().year();
        	mIdUserVacaciones.put(vacacion.Empleado__c, vacacion);
        	//System.Debug('NombreVACACION: ' + vacacion);
		}

		Vacaciones__c vacacionesRec;

		// Falta crear los registros que no tenian vacaciones. Comprobamos qué usuarios no estan en el mapa
		for(User usuario : listNew){
			if(!mIdUserVacaciones.containsKey(usuario.Id)){
				vacacionesRec = new Vacaciones__c(Empleado__c = usuario.Id, Activo__c = true);
        		vacacionesRec.Dias_de_vacaciones__c = getDiasVacaciones(usuario.Fecha_de_incorporacion__c);
        		vacacionesRec.Anyo__c = Date.today().year();
        		vacacionesRec.Name='Vacaciones de ' + usuario.FirstName + ' ' + usuario.LastName +  ' ' + Date.today().year();
        		mIdUserVacaciones.put(usuario.Id, vacacionesRec);
        		//System.Debug('NombreUSUARIO: ' + usuario);
			}
		}

        upsert mIdUserVacaciones.values();
	}

    public static Integer getDiasVacaciones(Date fechaIncorporacion){
        Integer diasVacaciones=22;
		Decimal vacacionesPorDia = Decimal.valueOf(diasVacaciones).divide(365,7);
		Date anyoActual = Date.newInstance(Date.today().year(),12,31);
        
 		if(fechaIncorporacion.year()==anyoActual.year()){
  			diasVacaciones = Integer.valueof((fechaIncorporacion.daysBetween(anyoActual)
                              *vacacionesPorDia).round(System.RoundingMode.HALF_UP));
  		 }

        return diasVacaciones;
    }
}